package br.uff.id.barreto.timtecandroidapp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    //no onCreate é o momento de mostrar a interface ao usuário.
    //esta na superclasse, que já esta em activity. Estamos sobrescrevendo.
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //setContentView - exibe o layout
        //R.layout.activity_main --> é o layout (agora é vazio), gerado automaticamente pelo android.
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Mostra o menu. Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater() acessa o gerenciador de menus
        // inflate(R.menu.menu_main, menu) --> preenche o menu a partir das definições do recurso de menu(R.menu.menu_main, menu).
        // neste momento, o menu_main possui apenas uma opção que é o settings.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    //  onOptionsItemSelected(MenuItem item) processa cada opção/item de menu(por meio de switch).
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //  onOptionsItemSelected recebe um item de menu que possui um Id, um identicador.
        //esse id vem do R.id.

        //noinspection SimplifiableIfStatement
        // recuperamos esse id do item de menu que vem do recurso R.id.action_settings
        if (id == R.id.action_settings) {
            return true;//não farei nada =P
        }

        return super.onOptionsItemSelected(item);
    }
}
