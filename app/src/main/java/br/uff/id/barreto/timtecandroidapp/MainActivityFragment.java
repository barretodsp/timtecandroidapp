package br.uff.id.barreto.timtecandroidapp;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    public MainActivityFragment() {
    }

    @Override
    //cria a tela, para aparecer a telinha.
    //LayoutInflater inflater = o que mostra o layout.
    //ViewGroup container grupo que contem o layout.
    //savedInstanceState infos sobre app já em execução
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        // (R.layout.fragment_main especifica o layout que vai aparecer na interface.
        Button btnNumberGuessGame = (Button) rootView.findViewById(R.id.btnNumberGuessGame);

        btnNumberGuessGame.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NumberGuessGame.class);
                getActivity().startActivity(intent);

            }
        });

        return rootView;
    }

}
