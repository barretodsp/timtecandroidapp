package br.uff.id.barreto.timtecandroidapp;

import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * A placeholder fragment containing a simple view.
 */
public class NumberGuessGameFragment extends Fragment {

    Button btnNewGame = null;
    Button btnGuess = null;
    EditText inputGuess = null;
    TextView gameMessage = null;

    boolean gameFinished = false;
    int secretNumber = 0;



    public NumberGuessGameFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_number_guess_game, container, false);

        btnNewGame = (Button) rootView.findViewById(R.id.btnNewGame);

        btnNewGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionNewGame();
            }
        });

        btnGuess = (Button) rootView.findViewById(R.id.btnGuess);

        btnGuess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processGuess();
            }
        });

        inputGuess = (EditText) rootView.findViewById(R.id.inputGuess);
        gameMessage = (TextView) rootView.findViewById(R.id.textGameMessage);


        newGame();
        return rootView;


    }

    private void processGuess(){

        String strGuess = inputGuess.getText().toString();
        inputGuess.setText("");

        if(strGuess.length() == 0)
            return;

        int guess = Integer.valueOf(strGuess);

        if (guess > secretNumber ){

            gameMessage.setText(R.string.message_too_high);

        }
        else if(guess < secretNumber){

            gameMessage.setText(R.string.message_too_low);

        }
        else{
            gameMessage.setText(R.string.message_win);
            gameFinished = true;

        }

    }


    private void actionNewGame(){

        if(gameFinished){
            newGame();
            return;
        }

        new AlertDialog.Builder(getActivity())
                .setMessage(R.string.confirm_new_game)
                .setCancelable(false)
                .setNegativeButton(R.string.no, null)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        newGame();
                                            }

                }).show();


    }



    private void newGame(){

        secretNumber = (int) (Math.random() * 100);
        gameMessage.setText(R.string.message_welcome);
        inputGuess.setText("");
        gameFinished = false;


    }

}
